# How To Deploy A Docker Image To A Server Using GitLab CI/CD

This is the repository for the blog post you can find [here](https://www.programonaut.com/how-to-deploy-a-docker-image-to-a-server-using-gitlab-ci-cd/). In it you will learn how to automatically deploy a docker image to your server using GitLab CI/CD.

**PAT**: [https://gitlab.com/-/profile/personal_access_tokens?name=container+registry&scopes=write_registry,read_registry](https://gitlab.com/-/profile/personal_access_tokens?name=container+registry&scopes=write_registry,read_registry)